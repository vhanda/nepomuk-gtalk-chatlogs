/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2012  Vishesh Handa <handa.vish@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "parser.h"

#include <QtCore/QFile>
#include <QtCore/QRegExp>

#include <QtXml/QDomDocument>
#include <QtXml/QDomNode>

#include <kdebug.h>

Parser::Parser(QObject* parent)
    : QObject(parent)
{

}

void Parser::parse(const QString& filename)
{
    QFile file( filename );
    if( !file.open(QIODevice::ReadOnly) ) {
        kWarning() << "Could not open " << filename;
        return;
    }
    QTextStream ts( &file );

    QRegExp boundaryRegexp(QLatin1String("\\s*boundary=\"(.*)\""));
    QString boundary;

    while( !ts.atEnd() ) {
        QString line = ts.readLine();
        if( line.indexOf(boundaryRegexp) != -1 ) {
            boundary = boundaryRegexp.cap(1);
            break;
        }
    }

    boundary.prepend("--");
    // Find the boundary
    while( !ts.atEnd() ) {
        QString line = ts.readLine();
        if( line == boundary )
            break;
    }

    QString contentType = ts.readLine();
    int index = contentType.indexOf(QLatin1String("Content-Type:"), 0, Qt::CaseInsensitive);
    if( index != -1 )
        contentType = contentType.mid(index).trimmed();

    if( !contentType.contains(QLatin1String("text/xml")) ) {
        kWarning() << "The file doesn't contain xml";
        kWarning() << contentType;
        return;
    }

    QString encoding = ts.readLine();
    index = encoding.indexOf(QLatin1String("Content-Transfer-Encoding:"), 0, Qt::CaseInsensitive);
    if( index != -1 )
        encoding = encoding.mid(index).trimmed();

    if( !encoding.contains(QLatin1String("quoted-printable")) ) {
        kWarning() << "We only support quoted-printable encoding";
        return;
    }

    QString xmlContents;
    while( !ts.atEnd() ) {
        QString line = ts.readLine();
        if( line == boundary )
            break;

        if( line.endsWith('=') ) // soft break
            line = line.mid(0, line.size()-1);

        xmlContents.append(line);
    }

    // Convert from quoted-printable
    xmlContents = fromQuotedPrintable(xmlContents);

    // Parse the xml
    QDomDocument dom("conversation");
    dom.setContent(xmlContents);

    QDomElement root = dom.documentElement();
    if( root.tagName() != QLatin1String("con:conversation") ) {
        kWarning() << "Error in parsing xml - con:conversation expected. Got " << root.tagName();
        return;
    }

    QDomElement message = root.firstChildElement("cli:message");
    for( ; !message.isNull(); message = message.nextSiblingElement() ) {
        QString from = message.attribute("from");
        QString to = message.attribute("to");

        //FIXME: Actually use the namespaces
        QString text = message.firstChildElement("cli:body").text();
        if( text.isEmpty() )
            text = message.firstChildElement("ns2:body").text();

        QDomElement x = message.firstChildElement("x");
        QString dateTimeString = x.attribute("stamp");

        // Insert the '-' from the year and month
        dateTimeString.insert(4, '-');
        dateTimeString.insert(7, '-');

        // FIXME: Make sure the time is in UTC
        QDateTime dt = QDateTime::fromString(dateTimeString, Qt::ISODate);
        dt.setTimeSpec(Qt::UTC);

        emit newMessage(from, to, text, dt);
    }
}

QString Parser::fromQuotedPrintable(const QString& input)
{
    const int hexVal[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 10, 11, 12, 13, 14, 15};

    QByteArray output;

    for (int i = 0; i < input.length(); ++i) {
        if (input.at(i).toAscii() == '=') {
            output.append((hexVal[input.at(++i).toAscii() - '0'] << 4) + hexVal[input.at(++i).toAscii() - '0']);
        }
        else {
            output.append(input.at(i).toAscii());
        }
    }

    return QString::fromUtf8(output.data(), output.size());
}
