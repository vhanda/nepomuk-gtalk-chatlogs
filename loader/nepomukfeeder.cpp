/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2012  Vishesh Handa <handa.vish@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "nepomukfeeder.h"

#include <nepomuk/datamanagement.h>
#include <nepomuk/storeresourcesjob.h>
#include <nepomuk/simpleresource.h>
#include <nepomuk/simpleresourcegraph.h>

#include <Nepomuk/Vocabulary/NMO>
#include <Nepomuk/Vocabulary/NCO>

#include <KDebug>

using namespace Nepomuk::Vocabulary;

NepomukFeeder::NepomukFeeder(QObject* parent): QObject(parent)
{

}

void NepomukFeeder::slotMessage(const QString& from_, const QString& to_, const QString& text, const QDateTime& dt)
{
    if( text.isEmpty() )
        return;

    // Remove the /resourceId in someone@someplace.com/resourceId
    const QString from = from_.mid( 0, from_.indexOf('/') );
    const QString to = to_.mid( 0, to_.indexOf('/') );

    QUrl fromUri;
    if( m_contactHash.contains(from) )
        fromUri = m_contactHash.value(from);
    else {
        Nepomuk::SimpleResource fromAccount;
        fromAccount.addType( NCO::IMAccount() );
        fromAccount.setProperty( NCO::imID(), from );

        m_graph << fromAccount;
        m_contactHash.insert( from, fromAccount.uri() );
        fromUri = fromAccount.uri();
    }

    QUrl toUri;
    if( m_contactHash.contains(to) )
        toUri = m_contactHash.value(to);
    else {
        Nepomuk::SimpleResource toAccount;
        toAccount.addType( NCO::IMAccount() );
        toAccount.setProperty( NCO::imID(), to );

        m_graph << toAccount;
        m_contactHash.insert( to, toAccount.uri() );
        toUri = toAccount.uri();
    }

    Nepomuk::SimpleResource mes;
    mes.addType( NMO::IMMessage() );
    mes.setProperty( NMO::plainTextMessageContent(), text );
    mes.setProperty( NMO::messageFrom(), fromUri );
    mes.setProperty( NMO::primaryMessageRecipient(), toUri );
    mes.setProperty( NMO::receivedDate(), dt );
    mes.setProperty( NMO::sentDate(), dt );

    Nepomuk::SimpleResourceGraph graph;
    m_graph << mes;
}

bool NepomukFeeder::commit()
{
    kDebug() << "Pushing ..";
    KJob * job = Nepomuk::storeResources( m_graph );
    job->exec();

    if( job->error() ) {
        kWarning() << job->errorString();
        return false;
    }

    kDebug() << "Done";
    return true;
}

