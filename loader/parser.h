/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2012  Vishesh Handa <handa.vish@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef PARSER_H
#define PARSER_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QDateTime>

class Parser : public QObject
{
    Q_OBJECT
public:
    explicit Parser(QObject* parent = 0);

    void parse(const QString& filename);

Q_SIGNALS:
    void newMessage(const QString& from, const QString& to, const QString& body, const QDateTime& dt);

private:
    QString fromQuotedPrintable(const QString &string);
};

#endif // PARSER_H
